'use strict';

/**
 * @ngdoc directive
 * @name directiveApp.directive:docker
 * @description Directive to making nice Apple like docker on the bottom of the site
 * # docker
 */
angular.module('directiveApp')
    .directive('ksDocker', function() {
        return {
            templateUrl: '/scripts/directives/docker.html',
            restrict: 'E',
            transclude: true,
            scope: true,

            link: function postLink(scope, element, attrs, $parse, $scope) {

                var options = {};
                var docker;

                var options = attrs.options || {};

                var defaults = {

                    activeSize: '200',
                    passiveSize: '100'


                };
                var settings = {};
                var settings = angular.extend({}, defaults, options);


                docker = element.find('.docker')
                docker.childs = docker.children();



                var handlers = function handlers() {
                    docker.childs.hover(isActive, isDeactive);

                }

                /**
                 * @param  {object} event object
                 *
                 * Handling mouse move on active element
                 */
                var isActive = function(e) {

                    var currentElem = e.currentTarget;
                    console.log(currentElem)
                    var element = angular.element(currentElem);

                    var elem = { 
                    	element : currentElem,
                    	context : 	element,
                    	next : element.next(),
                    	prev : element.prev()
                    };

                    elem.context.on('mousemove', mousemoveRegister.bind(elem));

                    
                }
                /**
                 * @param  {object} event object
                 * deataching event handler after mouseout
                 */
                var isDeactive = function(e) {
                    var currentElem = e.currentTarget;

                    var elem = angular.element(currentElem);
                    elem.off('mousemove', mousemoveRegister);
                    scope.$emit('mouseout');

                    return this;


                }
                /**
                 * @param  event {object}
                 * @param  context {object}
                 *
                 * Function are calculating centerRatio mouse position on active Element
                 */
                var mousemoveRegister = function(event, context) {

                    var positionPropertis = this
                        .element
                        .getBoundingClientRect();


                    this.elementWidth = positionPropertis.width;

                    this.offsetLeft = positionPropertis.left;
                    this.xPosition = event.pageX - this.offsetLeft;

                    var centerRatio = ((this.xPosition - settings.activeSize / 2) / settings.activeSize / 2);
                    var positionRatio = this.xPosition / this.elementWidth;


                    //pubsub event emiting. To avoid hardcoded function
                    scope.$emit('centerRatioChange', [this, centerRatio]);
                }
                /**
                 * @param  {obj}
                 * @param attrs {array} array of attrs
                 * 
                 * Controlling elements before and after active one
                 */
                var prevNextSizeController = function(d, attrs) {

                    var element = attrs[0];
                    var centerRatio = attrs[1];


                    element.next.css({
                        width: (0.75 + centerRatio) * settings.activeSize,
                        height: (0.75 + centerRatio) * settings.activeSize
                    });
                    element.prev.css({
                        width: (0.75 - centerRatio) * settings.activeSize,
                        height: (0.75 - centerRatio) * settings.activeSize
                    });
                    
                }
                /**
                 * Resize element after mouse out
                 */
                var elementSizeReset = function() {
                    console.log('reset');
                    docker.childs.removeAttr('style');

                }

                //pubsub event handlers
                scope.$on('mouseout', elementSizeReset);
                scope.$on('centerRatioChange', prevNextSizeController)
                handlers();



            }
        };
    });

/**
 *  Docker element directive
 */
angular.module('directiveApp')
    .directive('ksDockerElement', function() {
        return {

            restrict: 'E',
            transclude: true,
            templateUrl: '/scripts/directives/docker-elem.html',
            scope: {},
            link: function postLink(scope, element, attrs) {

                attrs.$set('class', 'docker__item');
                console.log(attrs.icon)

                scope.imageSrc = attrs.icon;




                element.on('mouseenter', function() {
                    element.addClass('active');
                });
                element.on('mouseleave', function() {
                    element.removeClass('active');
                });





            }
        };
    });