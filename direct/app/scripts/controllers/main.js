'use strict';

/**
 * @ngdoc function
 * @name directiveApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the directiveApp
 */
angular.module('directiveApp')
  .controller('MainCtrl', function ($scope) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
      });
