'use strict';

describe('Directive: docker', function () {

  // load the directive's module
  beforeEach(module('directiveApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<docker></docker>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the docker directive');
  }));
});
